# GNOME Sticker

## Getting started

I designed a GNOME Sticker for Global events, designed with Inkscape and you can use the .SVG
This is part of the Marketing collaboration for the GNOME project made with this amazing Inkscape Application.

- [GNOME](https://www.gnome.org/): is a graphical user interface (GUI) and set of computer desktop applications for users of the Linux operating system. It's intended to make a Linux operating system easy to use for non-programmers and generally corresponds to the Windows desktop interface and its most common set of applications.

- [Inkscape](https://inkscape.org/): is a Free and open source vector graphics editor for GNU/Linux. It offers a rich set of features and is widely used for both artistic and technical illustrations such as cartoons, clip art, logos, typography, diagramming and flowcharting. It uses vector graphics to allow for sharp printouts and renderings at unlimited resolution and is not bound to a fixed number of pixels like raster graphics. Inkscape uses the standardized SVG file format as its main format, which is supported by many other applications including web browsers.

- GNOME Sticker

![alt text](https://gitlab.gnome.org/dnlgalleguillos/gnome-sticker/-/raw/main/GNOME_Stickers.png)
